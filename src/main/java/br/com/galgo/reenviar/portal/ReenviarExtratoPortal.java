package br.com.galgo.reenviar.portal;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.reenviar.ReenviarInformacoes;
import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Grupo;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTransf;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class ReenviarExtratoPortal extends ReenviarInformacoes {

	Teste teste;
	private final String PASTA_TESTE = "ReenvioExtratoPortal";

	@Before
	public void setUp() throws Exception {
		Ambiente ambiente = TesteUtils.configurarTeste(Ambiente.HOMOLOGACAO,
				PASTA_TESTE);
		List<Teste> listaTeste = ArquivoUtils.getListaTeste(ambiente,
				ConstantesTestes.CAMINHO_TESTE_TRANSF,
				ConstantesTransf.ABA_CASOS_TESTE_TRANSF,
				ConstantesTransf.LINHA_INICIAL_TESTE_TRANSF,
				ConstantesTransf.COLUNA_GRUPO_TRANSF,
				ConstantesTransf.COLUNA_SERVICO_TRANSF,
				ConstantesTransf.COLUNA_CANAL_TRANSF,
				ConstantesTransf.COLUNA_ID_TRANSF,
				ConstantesTransf.COLUNA_OPERACAO_TRANSF,
				ConstantesTransf.COLUNA_DESCRICAO_TRANSF,
				ConstantesTransf.COLUNA_RETESTE_TRANSF);
		Grupo grupo = Grupo.ANTES;
		Servico servico = Servico.EXTRATO;
		Operacao operacao = Operacao.REENVIO_INFORMES;
		Canal canal = Canal.PORTAL;

		teste = Teste.fromListaTeste(listaTeste, ambiente, grupo, servico,
				operacao, canal);
	}

	@Test
	public void reenviar() throws Exception {
		reenviarInformacao(teste);
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste("reenvioExtratoPortal.png");
	}
}
