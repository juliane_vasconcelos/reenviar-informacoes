package br.com.galgo.reenviar;

import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.MassaDados;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaExtrato;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaPLCota;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvio;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioExtrato;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioExtratoArquivo;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioPlCota;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioPlCotaArquivo;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioPosicaoArquivo;
import br.com.galgo.testes.recursos_comuns.pageObject.reenvio.TelaReenvioInfoAnbima;
import br.com.galgo.testes.recursos_comuns.pageObject.reenvio.TelaReenvioPlCota;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;

public class ReenviarInformacoes extends TelaGalgo {

	private Teste teste;
	private String caminhoMassaDadosFundos;

	public void reenviarInformacao(Teste teste) throws Exception {
		this.teste = teste;
		Ambiente ambiente = teste.getAmbiente();
		caminhoMassaDadosFundos = MassaDados.fromAmbiente(ambiente,
				ConstantesTestes.DESC_MASSA_DADOS_TRANSF).getPath();

		String login = ArquivoUtils.getLogin(teste, caminhoMassaDadosFundos);
		String senha = ArquivoUtils.getSenha(teste, caminhoMassaDadosFundos);
		Usuario usuario = new Usuario(login, senha, ambiente);

		reenviarInformacao(usuario);
	}

	private void reenviarInformacao(Usuario usuario) throws Exception {
		Ambiente ambiente = teste.getAmbiente();
		Servico servico = teste.getServico();
		Canal canal = teste.getCanal();
		int qtdReteste = teste.getQtdReteste();

		int qtdExecucoes = 1;
		TelaGalgo.abrirBrowser(ambiente.getUrl());
		TelaLogin telaLogin = new TelaLogin();
		telaLogin.loginAs(usuario);

		while (qtdExecucoes <= qtdReteste) {
			String codigoSTI = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, 1, qtdExecucoes);
			String dataBase = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, 2, qtdExecucoes);

			String codigoSTICotista = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, false, 3, qtdExecucoes);

			reenviar(usuario, servico, canal, dataBase, codigoSTI,
					codigoSTICotista);
			qtdExecucoes++;
		}
	}

	private void reenviar(Usuario usuario, Servico servico, Canal canal,
			String dataBase, String codigoSTI, String codigoSTICotista)
			throws Exception {
		if (Servico.EXTRATO == servico) {
			reenviarExtrato(usuario, canal, dataBase, codigoSTI,
					codigoSTICotista);
		} else if (Servico.PL_COTA == servico) {
			reenviarPlCota(usuario, canal, dataBase, codigoSTI);
		} else if (Servico.INFO_ANBIMA == servico) {
			reenviarInfoAnbima(usuario, canal, dataBase, codigoSTI);
		} else if (Servico.POSICAO_ATIVOS == servico) {
			reenviarPosicao(usuario, dataBase, codigoSTI);
		}
	}

	private void reenviarExtrato(Usuario usuario, Canal canal, String dataBase,
			String codigoSTI, String codigoSTICotista) throws Exception {
		TelaHome telaHome = new TelaHome(usuario);
		TelaEnvioExtrato telaEnvioDeExtrato = (TelaEnvioExtrato) telaHome
				.acessarSubMenu(SubMenu.ENVIA_EXTRATO);
		if (Canal.PORTAL == canal) {
			telaEnvioDeExtrato = new TelaEnvioExtrato(dataBase, codigoSTI,
					codigoSTICotista);
			TelaConsultaExtrato telaConsultaExtrato = telaEnvioDeExtrato
					.clicarBotaoBuscar();
			telaConsultaExtrato
					.consultar(codigoSTICotista, codigoSTI, dataBase);
			telaConsultaExtrato.escolherPrimeiroItem();
			telaEnvioDeExtrato = telaConsultaExtrato.clicarBotaoEnviar(
					dataBase, codigoSTI, codigoSTICotista);
			telaEnvioDeExtrato.clicarMaisSaldoConsolidado();
			telaEnvioDeExtrato
					.preencherCamposValoresAnteriores(TelaEnvio.VALOR_REENVIO);
			telaEnvioDeExtrato.clicarBotaoEnviar();
			telaEnvioDeExtrato.validaReenvio();
		} else if (Canal.ARQUIVO == canal) {
			TelaEnvioExtratoArquivo telaEnvioExtratoArquivo = telaEnvioDeExtrato
					.clicarUpload(usuario, dataBase, codigoSTI,
							codigoSTICotista);
			telaEnvioExtratoArquivo.enviar(Operacao.REENVIO_INFORMES);
		}
	}

	private void reenviarPosicao(Usuario usuario, String dataBase,
			String codigoSTI) throws Exception {
		TelaHome telaHome = new TelaHome(usuario);
		TelaEnvioPosicaoArquivo telaEnvioPosicaoArquivo = (TelaEnvioPosicaoArquivo) telaHome
				.acessarSubMenu(SubMenu.ENVIA_POSICAO);
		telaEnvioPosicaoArquivo = new TelaEnvioPosicaoArquivo(usuario,
				dataBase, codigoSTI);
		telaEnvioPosicaoArquivo.enviar(Operacao.REENVIO_INFORMES);

	}

	private void reenviarInfoAnbima(Usuario usuario, Canal canal,
			String dataBase, String codigoSTI) throws Exception {
		TelaHome telaHome = new TelaHome(usuario);
		TelaConsultaPLCota telaConsultaPLCota = (TelaConsultaPLCota) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_PL_COTA);
		telaConsultaPLCota.selecionarFilttroStatusEnviada();
		telaConsultaPLCota.consultar(codigoSTI, dataBase);
		TelaReenvioInfoAnbima telaReenvioInfoAnbima = new TelaReenvioInfoAnbima(
				codigoSTI, dataBase);
		if (Canal.PORTAL == canal) {
			telaReenvioInfoAnbima.reenviar();
		} else if (Canal.ARQUIVO == canal) {
			TelaEnvioPlCota telaEnvioPlCota = (TelaEnvioPlCota) telaHome
					.acessarSubMenu(SubMenu.ENVIA_PL_COTA);
			TelaEnvioPlCotaArquivo telaEnvioPlCotaArquivo = telaEnvioPlCota
					.clicarUploadArquivo(usuario,
							TelaReenvioPlCota.CAMINHO_XML_REENVIO_INFO,
							codigoSTI, dataBase, Operacao.REENVIO_INFORMES);
			telaEnvioPlCotaArquivo.enviar();
		}
	}

	private void reenviarPlCota(Usuario usuario, Canal canal, String dataBase,
			String codigoSTI) throws Exception {
		TelaHome telaHome = new TelaHome(usuario);
		TelaConsultaPLCota telaConsultaPLCota = (TelaConsultaPLCota) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_PL_COTA);
		telaConsultaPLCota.selecionarFilttroStatusEnviada();
		telaConsultaPLCota.consultar(codigoSTI, dataBase);
		TelaReenvioPlCota telaReenvioPlCotaPortal = new TelaReenvioPlCota(
				codigoSTI, dataBase);
		if (Canal.PORTAL == canal) {
			telaReenvioPlCotaPortal.reenviar();
		} else if (Canal.ARQUIVO == canal) {
			telaHome.acessarSubMenu(SubMenu.ENVIA_PL_COTA);
			telaReenvioPlCotaPortal.reenviarArquivo(usuario,
					TelaReenvioPlCota.CAMINHO_XML_REENVIO_PL_COTA, dataBase,
					codigoSTI);
		}
	}

}