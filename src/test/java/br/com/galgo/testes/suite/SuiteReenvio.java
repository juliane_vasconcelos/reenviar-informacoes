package br.com.galgo.testes.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.reenviar.arquivo.ReenviarExtratoArquivo;
import br.com.galgo.reenviar.arquivo.ReenviarInformacaoAnbimaArquivo;
import br.com.galgo.reenviar.arquivo.ReenviarPlCotaArquivo;
import br.com.galgo.reenviar.arquivo.ReenviarPosicaoAtivosArquivo;
import br.com.galgo.reenviar.portal.ReenviarExtratoPortal;
import br.com.galgo.reenviar.portal.ReenviarInfoAnbimaPortal;
import br.com.galgo.reenviar.portal.ReenviarPlCotaPortal;
import br.com.galgo.testes.recursos_comuns.suite.StopOnFirstFailureSuite;

@RunWith(StopOnFirstFailureSuite.class)
@Suite.SuiteClasses({ ReenviarPlCotaPortal.class,//
		ReenviarInfoAnbimaPortal.class,//
		ReenviarExtratoPortal.class, //
		ReenviarPlCotaArquivo.class,//
		ReenviarInformacaoAnbimaArquivo.class,//
		ReenviarExtratoArquivo.class,//
		ReenviarPosicaoAtivosArquivo.class //
})
public class SuiteReenvio {

}
